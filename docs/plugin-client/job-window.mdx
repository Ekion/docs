---
title: Plugin/Client Job Window
sidebar_label: Job Window
slug: /plugin-client/job-window
---

The **Job** window accompanies you throughout your job on FSCharter. Most of your interaction with the plugin/client will be using this window.

## No Active Job

If you have not yet accepted a job from the [FSCharter Marketplace](https://fscharter.net/marketplace) the **Job** window will notify you and give you the option to open the Marketplace in a browser:
![Job Window - No active Job](img/JobWnd_NoJob.png)

For more information about the job Marketplace, see [here](/docs/routes-and-jobs/marketplace-overview).

## Begin Job

Once you have accepted a job on the FSCharter platform the plugin will give you the option to begin the job:
![Job Window - Active Job](img/JobWnd_BeginJob.png)


:::info Conditions for beginning a job
The aircraft must be **parked on the ground** with the **parking brake on** and the **engines off** to begin a job. For exceptions see [here](settings-window#plane-specific-options)
:::

When you click on `Begin Job` your aircraft will be moved to the last known position within FSCharter's persistent world.

If the **Location** setting is turned off in [plane-specific options](settings-window#plane-specific-options) then you'll need to taxi to the last known position yourself. An arrow will point you in the right direction.

## Boarding

Boarding an aircraft loads the aircraft with the allocated fuel, passengers, and cargo for the job. By default the plugin/client will attempt to automatically set payload weight and fuel when you click `Board`.
![Job Window - Board](img/JobWnd_Board.png)

:::info Conditions for boarding a job
The aircraft must be **parked on the ground** with the **parking brake on**, the **engines off** and must be **located within 150m of the last known position** in FSCharter to board a job.
:::

:::caution Ensuring safe load is the Pilot's Responsibility
**As the pilot in command you are responsible for correct loading and balancing of the aircraft before departure.** The plugin/client will attempt to automatically load fuel and payload but will not control the centre of gravity (CG) or be aware of the aircraft's weight limits. Always pay attention to your weight limits whilst configuring the job. The plugin/client can bring an aircraft out of balance and potentially make it unsafe (e.g. by tilting up due to bad CG after automatic loading). You will need to redistribute load in such cases yourself.
:::

### Fuel Loading
Already when [configuring your job](/docs/routes-and-jobs/marketplace-overview#accepting-a-job) on FSCharter, make sure to have enough fuel. See [tips like this](https://aviation.stackexchange.com/questions/3740/what-are-the-icao-fuel-reserve-requirements/3742#3742) to learn what the pros do. If you trip is too long you can plan ahead for [stopovers](#stopover).

The plugin/client will distribute fuel proportionally across all tanks based on the tanks' sizes.

### Payload Loading

**Total payload** is cargo plus the weight of all passengers plus one pilot (each person weighs 70kg in FSCharter):
```
Total Payload = Cargo + 70kg * (Pax + 1)
```

Also see [this FAQ](faq#cannot-set-payload-or-fuel-to-expected-values) on some common payload loading issues.

#### X-Plane

In **X-Plane**, the plugin can only generically set the total payload and is not able to influence distribution at all. You will need to use your 3rd party tools to redistribute load and ensure a safe center of gravity (CG).

#### MSFS / P3D

In **MSFS/P3D**, payload is assigned to _payload stations_, which can range in size from the pilot seat or small baggage compartments to the entire cabin or cargo compartments. **The client does not have awareness of the maximum permitted weight per station** and may assign too much weight to some stations causing a fatal shift in center of gravity (CG). Any automatic payload assignment can only be considered an educated guess. The client works as follows:

- Based on the payload station's names (which are assigned by the model's developer) the client tries to identify individual person's seats and passenger cabins, while everything else is considered cargo. 
- Distribute weight of pax by
    - assigning `70kg` to each _individual person seat_ and then
    - distributing the remaining pax weight, if any, evenly across all _cabins,_ if there are any.
- Distribute remaining payload, if any,
    - to all _cargo_ payload stations, if there are any,
    - across all payload stations, no matter which type, otherwise; the latter ensures that all payload is eventually distributed somewhere.

The [Simulator Info Window](sim-info-window) shows all payload stations and their assigned weights after boarding. Open the "Payload" section for details. Name suffix "/P" denotes a single person, "/C" a cabin.

#### Manual Loading

If the **Fuel** or **Payload** options are disabled in the [plane-specific options](settings-window#plane-specific-options), then you'll need to set fuel levels and payload yourself, typically using the aircraft's specific loading screens. The **Job** window tells you required values:
![Job Window - Fuel/Payload manually](img/JobWnd_ManualFuelPayload.png)

There are tolerances for total fuel and payload, so you don't need to match expected levels to the digit. As soon as the above messages disappear you are good to go.

:::caution The flight will not start with incorrect fuel or payload levels
If fuel or payload is still at unacceptable levels upon takeoff then the plugin/client will refuse to acknowledge the flight. You will need to start over.
:::

## Takeoff

After boarding you can finish your flight preparation, **double check your balance and CG**, start your engines, taxi to the runway, and takeoff.
![Job Window - Taxi to runway and takeoff](img/JobWnd_BoardingComplete.png)

Actual takeoff is announced via a message popup:
![Message Window - Take off](img/MsgWnd_FlightStarted.png)

:::info Conditions for takeoff
The aircraft must **be located reasonably close to the boarding position**, have **a valid amount of fuel** (or less), have **a valid amount of payload** (or more), be **off the ground**, and be **faster than 35kn or higher than 50ft above ground** to log a takeoff.
:::

## In Flight

During the flight the **Job** window offers some statistics. The **Expected fuel level** is what FSCharter thinks you should have in your tanks. Essentially this is the fuel configured for the job minus fuel consumed.

The link symbol in the window bar opens the flight status page on the FSCharter platform, displaying your flight path.
![Job Window - In Flight](img/JobWnd_InFlight.png)

:::info Conditions for valid flight
**Fuel level** must remain at or below the expected level, **payload** must remain at or above expectation, **ground speed** must not exceed a type-dependent maximum speed (prevents time compression/sim rate increase and foul play with too fast aircraft types), **no slewing**, and **the aircraft must not crash** to remain in valid flight.
:::

If the **Fuel** setting is turned off in [plane-specific options](settings-window#plane-specific-options), then you can change fuel mid-flight (even increase the fuel level provided total fuel stays below _expected_ fuel level). This can be handy after [resuming a flight](/docs/flights/resuming-a-flight) to compensate for the fuel burned while going to the resume point.

## Landing

Landing is announced by a popup message. The [Landing Rate](/docs/plugin-client/faq#how-is-the-landing-rate-determined) appears in the **Job** window:
![Job Winodw - Landing](img/JobMsgWnd_Landed.png)

:::info Conditions for landing
The aircraft must be **on the ground**, be **travelling slower than 35 knots**, and have had a **landing rate** of `> -750ft/m`.
:::

After landing taxi to a proper parking location.

### Stopovers

If you did not land at your intended destination airport then the job will be placed in a **Stopover** state and the plugin will react accordingly. See [here](#stopover) for more information on how to use the plugin during a stopover.

## Disembarking

:::caution Properly Park the Aircraft
Make sure to reach a **proper parking location** off the runway. In FSCharter's _persistent world_ the next flight will start exactly where you left off and may be flown by a different pilot.
:::

:::info Conditions for disembarkation
Disembarking happens automatically when the aircraft **comes to a complete standstill**, **has the parking brakes applied** and **the engines are switched off**. Disembarkation must happen at a **reasonable distance from landing**, ie. at the same airport.
:::

If the **Parking Brake** setting is off or the **Engines On** setting is on in [plane-specific options](settings-window#plane-specific-options) then you don't need to set the parking brake and can leave your engines running. By doing this you will need to click on the provided `Disembark` button after stopping to a standstill to manually invoke a disembarkation.

:::caution Disembark for Completion
Remember, you are only paid after disembarking!
:::

![Job Window - Job Completed](img/JobWnd_Completed.png)

The `Job Details` button takes you to FSCharter's Job page of the recently completed job to review the job and check your earnings.

If you accepted a return job it will automatically be shown after 8 seconds. Simply return to [Begin Job](#begin-job) and follow this process to complete the inbound job.

## Stopover

Running out of fuel? Destination unreachable due to heavy weather? Or both? Consider a stopover at a safe airport. No need to tell FSCharter in advance, just land elsewhere. As soon as you land at any airport that is not the job's destination the job will enter a **Stopover** state.

The **Job** window will indicate if you have performed a stopover by displaying a "This is going to be a Stopover" message.

Taxi to a parking position (or the fuel truck) and stop there as if you would disembark (by setting the parking brake and cutting your engines). The **Job** window displays a "Stopover: Configure fuel on FSCharter, then continue to..." message.

![Job Window - Parked for Stopover](img/JobWnd_Stopover_Parked.png)

:::tip Take a Break
At this point you could take a break for up to 12 hours (until takeoff), switch off the simulator and come back later. The same unfinished job will be offered to you upon restart.
:::

Click on the `Open Active Job` button to open the job page on FSCharter. From there click `Configure Stopover`, set your new fuel level and confirm. A moment later, the **Job** window will update to show the configured fuel and gives you the option to resume:

![Job Window - Resume Stopover](img/JobWnd_Stopover_Resume.png)

Click on `Resume after Stopover` to continue and follow the same process as [boarding](#boarding).
