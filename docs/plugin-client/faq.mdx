---
title: Plugin/Client FAQ
sidebar_label: FAQ
slug: /plugin-client/faq
---

## General

### Interruption vs Invalidation

Jobs in FSCharter are subject to certain constraints so that all users share a fair environment. The FSCharter client and server both continuously validate these constraints.

If any of the constraints are no longer satisfied then three different outcomes are possible:
- **Simulator paused:** This allows you to remedy the situation immediately (adjust fuel, payload, etc.) and then unpause the simulator and continue the flight.
- **Flight interrupted** (locally): The FSCharter client stops tracking your flight, which is still valid on the FSCharter server. Premium users can click `Start Over` and [resume the flight](/docs/flights/resuming-a-flight#other-typical-scenarios) assuming the constraint that triggered the interruption is no longer active. Corrective actions (e.g. position) will happen as part of the flight resume procedure.
- **Job invalidation** (centrally): If the server finds one of its [constraints](/docs/flights/invalidated-flights) are broken it will _invalidate_ the flight directly on the server, which is unrecoverable.

Here is an overview how each rule is handled:

The simulator will be automatically paused when:
- The fuel level of the aircraft is higher than expected.
- The payload of the aircraft is too low

The flight will be locally **Interrupted** (not invalidated) when:
- Position manipulation is deteceted due to 'slewing'.
- Position manipulation is detected due to change of location via the menu or the map.
- The main menu is accessed in MSFS.

The flight will be irreversably [invalidated](/docs/flights/invalidated-flights) when:
- The ground speed of the aircraft is too high ([See here for more information on time acceleration](#invalidated-for-being-too-fast-or-trying-to-use-time-acceleration)).
- The vertical speed is too high.
- The aircraft has flown much further than its listed range.
- The takeoff point is too far from the boarding point.
- The aircraft has landed at an airport that is not known to FSCharter.
- The simulator has reported that the aircraft has crashed.
- FSCharter detects a crash due to an extremely hard landing.
- The flight has been paused for too long.
- The route has been removed buy the manager.
- The pilot has deleted their FSCharter account.

### Invalidated for being too fast or trying to use time acceleration

If your flight is invalidated with the reason "The aircraft is travelling too fast." then the server thinks that you are travelling faster than reasonable in the type of aircraft allocated to the configured job. Each aircraft has a maximum permitted speed, against which the server compares your actual speed (with quite some margin to cover for wind, etc).

If your flight was invalidated due to it being too fast then one of the following could apply:

- You were using time acceleration in the simulator. **Time acceleration is not allowed in FSCharter.** All hours flown are real-world hours.
- You had an incredibly strong tail wind, in which case our margins may require tweaking. Please [file a Feedback ticket](support) attaching your logs.
- The maximum speed stored in FSCharter's database is wrong. (If you are a company owner you can look up aircraft details when trying to [buy one](/docs/companies/operations-cockpit/managing-aircraft/purchasing).) Again, please [file a Feedback ticket](support) attaching your logs.

### Parking Breake not recognized

If the FSCharter client does not recognize that your aircraft's parking brake is set, then go to [Settings > Plane-specific options](settings-window#plane-specific-options) and disable the "Parking Brake" option.

### Cannot set Payload or Fuel to expected values

The FSCharter client compares the payload and fuel levels of your simulated aircraft against what you have configured in your FSCharter job. If it finds that your aircraft has **too little payload** or **too much fuel** then it will show a warning that the **flight cannot be started** if values are not corrected before take-off. However, if the client finds that your aircraft has too much payload or too little fuel then you will see a hint that you _may_ adjust payload/fuel, but you are not required to before starting the flight.

Consider the following when having difficulties matching the values between your aircraft and the FSCharter job:

- From a payload perspective, each passenger adds `70kg` of mass, as does the pilot. 3rd party aircraft may feature complex loading settings to distribute passengers and may calculate a different weight per passenger. Ultimately, you need to match _total weight of payload._ The number of passengers configured in a 3rd party loading screen is irrelevant to FSCharter. The [FSCharter Job Window](job-window#boarding) shows the expected total payload weight in the _Active Job_ section.
- FSCharter uses kg for payload and fuel so ensure that you're working in the correct units. The FSCharter client can also display converted units, see [Settings](settings-window).
- The simulated aircraft may allow less payload/fuel than the version set up in the FSCharter platform. In that case if you load the job to full allowed capacity on the FSCharter platform you may find that your aircraft cannot actually hold that capacity. In that case it is advisable to cancel the job on the FSCharter platform, configure a new job, then execute the "Start Over..." [menu item](overview#menu-items) in the FSCharter client to load the new job details. Always consider the capabilities of the simulated aircraft that you want to fly when you configure your job! Consider the maximum allowance on FSCharter only a guideline.
- There are 3rd party aircraft that report unusual weights as payload (E.g. it seems X-Plane's Zibo Mod B737 reports galley items as payload). The FSCharter client cannot determine such unusual usage, instead only seeing the resulting high payload value. In this case the plugin will likely hint to you that you can reduce payload. If you play "fair" then you can ignore the FSCharter client's hint. You _are_ allowed to fly with _more payload_ (or _less fuel_, ie. with disadvantegous configurations) to increase realism in these cases.

### How is the Landing Rate determined?

The FSCharter Client takes samples of your aircraft's flight data as reported by your simulator every 0.5 seconds. The vertical speed from the _last sample before ground contact_ is used as landing rate and forwarded to the server.

Based on this value the server decides on a hard landing, which causes a hard landing (HL) check, or on a crash landing, which causes [flight invalidation](/docs/flights/invalidated-flights).

As of March 2022 both of these limits are set to `-1000ft/min` pending new decisions and/or a redesign of the logic. That means triggering a HL is not currently possible. Landing with more than `-1000ft/min` causes flight invalidation due to a crash.

### Not enough fuel

Do you often end up with too little fuel? Watch the units! The [FSCharter platform](https://fscharter.net/marketplace) works with `kg` by default. If you enter `lb` figures into a `kg` field you will end up with too little fuel, as happened to the [Gimli Glider](https://en.wikipedia.org/wiki/Gimli_Glider).

If you are used to the Imperial System you can switch the _display_ of weights on the FSCharter platform to `lb` in the [site's preferences](https://fscharter.net/preferences). In the FSCharter Client you can change the display units for fuel (and payload) in the [Settings](settings-window#miscellaneous).

Or you use converters like [this](http://mye6b.com/Fuel/) to convert many more units, also volumetric units, to `kg`.

### Aircraft tilts up when boarding

You've probably taken on too much payload and definitely did not (yet) redistribute it properly in your weight and balance settings of your particular aircraft. As the pilot in command you are responsible for proper loading of the plane. That starts with configuring the job. Don't rely too much on the max weight indicators of the FSCharter platform! Check against the actual weight limitations of _your_ aircraft.

For more details check out [Job | Boarding](job-window#boarding).

### Aircraft does not teleport to boarding position

Firstly, make sure that _Location_ is still _enabled_ in the [Plane-specific options](settings-window#plane-specific-options).

Another gotcha might be vendor-specific gimmics. You may need to untie the plane and remove the chocks before accepting the job otherwise the plugin may not be able to reposition the aircraft. For example, the AirfoilLabs B350 is known to not allow any teleporting whilst the chocks are still in place.

### How do I pause a flight?

You can pause your flight anytime using your simulator's standard mechanisms for pausing. Pausing time will _not_ count towards your FSCharter flight time. There are basically 3 options:

#### Pause the Simulator

You can just pause your simulator for up to 6 hours. In X-Plane hit the `P` key, in MSFS the safest bet is to hit the `Esc` key to open the options menu. The FSCharter plugin/client shows a notification that the simulator is paused and the time remaining until [invalidation](/docs/flights/invalidated-flights).

#### Interrupt the Flight (Premium users only)

As a premium user you can make use of the flight resume functionality and just exit from the simulator altogether. In X-Plane, this is indeed the safest way: Just exit the sim. Using the SimConnect client for MSFS/P3D it is safer to first exit the SimConnect client (which will show a warning that a flight is in-progress) before exiting from the sim. The flight resume function was originally been designed to cover sim crashes, so don't worry.

[Resume your flight](/docs/flights/resuming-a-flight) (from the same computer or even a different one, you could even continue in a different simulator type) no later than 6 hours after you interrupted your flight.

#### How do I perform a stopover?

Land at any airport and FSCharter will consider this a [stopover](job-window#stopover), which can last up to 12 hours.

## SimConnect Client for MSFS/P3D

### How do I fly a job?

Here is a brief [Youtube Video](https://youtu.be/bfvMaV_p9KQ) about how to do a complete FSCharter flight with MS Flight Simulator.

### How do I resume a job?

This is a feature for premium users only. See a detailed description [here](/docs/flights/resuming-a-flight) and a [Youtube video](https://youtu.be/wRw11lJ9ZN8).

### Flight starts in a building

If you plane is teleported into a building when you click `Begin Job` it may be considered a crash in MSFS if `Crash Damage` is enabled. [See here](installing-sc-client#building-collision-msfs) for two options to solve this problem.

### Client freezes

Are you running screen capture, e.g. by Nvidia tools?

There have been reports that the FSCharter Client can freeze and subsequently take note of take-off too late, for example (leading to flight invalidation), while Nvidia screen capture tries to record a video.

### I get a "Crash Damage is off" message

You see the line "'Assistance Options > Failure and Damage > Crash Damage' is off." and wonder why?
This is just a little reminder to your conscience to play fair: When "Crash Damage" is off then landing upside down, banked 45 degrees, or with gear up would not be reported as a crash, though in real life it would harm your passengers. Currently, nothing else is going to happen. FSCharter is not currently enforcing to switch on "Crash Damage".

If you want to play fair, go to MSFS's _Assistance Options_ and enable _Failure and Damage > Crash Damage_.

### Can the FSCharter Client be the cause of MSFS' crashes/CTDs?

No, really highly unlikely.

We are willing to leave open a very tiny window of possibility as the world of software is complex. But before we assume the FSCharter Client can cause MSFS to crash a huge number of [other possibilities](https://www.google.com/search?q=microsoft+flight+simulator+crash+to+desktop) would need to be exhausted first. The reason is that the FSCharter Client is a seperate executable, an "out-of-process add-on" in Microsoft speak, running in a seperate process space. As a matter of fact, you can even [run the FSCharter Client on a different computer in the network](installing-sc-client#using-a-simconnect-remote-connection). Hence, the FSCharter Client does not in any direct way interact with MSFS' processes, threads, or memory. All communicaton is via [SimConnect](https://docs.flightsimulator.com/html/index.htm#t=Programming_Tools%2FSimConnect%2FSimConnect_SDK.htm), which uses a pipe or network connection.

The one small possibility for the _use_ of the FSCharter Client to trigger crashes is with MSFS' internal SimConnect implementation, ie. the endpoint FSCharter talks to: If MSFS chokes over data sent by the FSCharter Client or while compiling a response, then this would appear to go away if the FSCharter Client does not run. Still, it would not be in our hands to be fixed, but in Asobo's.

Please [report your bugs and crashes of MSFS to Asobo via Zendesk](https://flightsimulator.zendesk.com/hc/en-us/articles/360015913659-How-to-report-a-bug-). As we try to make FSCharter better based on your [support requests](support), also MSFS can only become better with your bug reports.

## X-Plane Plugin

### X-Plane crashes when beginning a job

If X-Plane crashes to desktop (CTD) when you click the `Begin Job` button in the [**Job** window](job-window#begin-job), then you might have fallen victim to a bug in X-Plane 11.55 (which has been reported to Laminar). An API call that would teleport you to the correct starting airport (whilst loading the correct scenery upfront) seems to fail fatally when called for a very small set of airports.

As we have a theory when this CTD happens, as of `v12.0.2` the FSC plugin tries to workaround the situation, having a list of potentially dangerous airports "in mind". Still, we cannot totally rule out the possibility of this CTD happening again.

To see if you have fallen victim to this bug look in your `Log.txt` file after the crash and check if it contains the following at the very end:

```
0:07:47.076 FSCharter DEBUG XP_Global.cpp:64/PlaceAtAiport: Placing user at airport WAYY
--=={This application has crashed because of the plugin: FSCharter}==--
```

FSCharter's `DEBUG` line will only show up if _logging in `Log.txt`_ has been configured to "Debug" level in [settings](settings-window#logging).

**Workaround**: Via X-Plane's main menu, load yourself at the correct airport _as reported in the above "Placing user at..." message_ ("WAYY" in the above log example) prior to clicking `Begin Job` so that the FSCharter plugin will not need to teleport you.
