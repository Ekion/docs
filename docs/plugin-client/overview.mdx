---
title: Plugin/Client Overview
sidebar_label: Overview
slug: /plugin-client/overview
---

FSCharter works by interacting with your simulator via an [X-Plane plugin](installing-xp-plugin) or the [SimConnect client](installing-sc-client), that connect your flight simulator with the [FSCharter platform](https://fscharter.net/). The plugin/client is responsible for placing your aircraft in the world, setting fuel and payload and recording takeoffs, flights and landings.

:::caution What the Plugin/Client NOT is
The FSCharter plugin/client is neither a load calculator, nor a flight planning app, nor an EFB, nor an FMS, nor an autopilot.
:::

It is "just" an interface between the plane you chose to fly and a platform with some economics. You are supposed to fly your planes as a pilot-in-command as close to real life as possible. You shall know by other professional means about the specifics of your plane, how to plan a flight and how to conduct it. Or in other words: You should be able to fly with the plugin/client window closed once boarded.

## Job Window ##

Most of the time you will be working with the **Job** window, which shows details about the current job, the current flight status, and possible next actions:
![Job Window - Begin Job](img/JobWnd_BeginJob.png)

[See here](job-window) for more details about the **Job** window.

## Settings ##

The **Settings** window is the place where you enter your FSCharter login credentials, have a few general options as well as a few so-called "aircraft-specific" options at the bottom:
![Settings Window](img/SettingsWnd.png)

[See here](settings-window) for more details about the **Settings** window.

## General UI Features ##

- **Mouse-over:** Most buttons and options show more details in a popup message when you hover your mouse over them.
- **Inactive buttons** explain why they are inactive in a mouse-over popup.
- **Section:** Small triangles indicate collapsable sections to save space. Click on the line to open/close the section.

![Sections, Popups](img/Sections_Popups.png)

- **? / Question Mark** opens a browser with context-sensitiv help here in the documentation.
- **Gear Wheel** in window's title bar opens a small popup window with UI settings.
- **Pop-out button** pops out the window into a separate operating system-level window, and back (_X-Plane_ only).
![Gear Wheel UI Settings](img/UI_GearWheel.png)

## Menu ##

### SimConnect Client ###

The client's menu bar is found at the top of the FSCharter client's window as usual. Added to its end is the status of the connection to the simulator; on mouse-over a popup shows more connection details:
![SimConnect Client menu bar](img/SC_Menu.png)

The following menu items are specific to the SimConnect client:

| Menu     | Item                   | Description                                                                                               |
|----------|------------------------|-----------------------------------------------------------------------------------------------------------|
| File     | Open Log Folder...     | Opens the folder with log files in a Windows explorer, useful for [support](installing-sc-client#support) |
| File     | Open Config Folder...  | Opens the folder with the client's configuration files in a Windows explorer                              |
| File     | Exit                   | Guess what...                                                                                             |
| Settings | Reconnect to Simulator | If the simulator connection was lost and then the sim restartet, this can reconnect the client to the sim |


### X-Plane Plugin ###

The plugin's menu is found in X-Plane's **Plugins** menu:
![X-Plane Plugin menu structure](img/XP_PluginMenu.png)

- A leading dot denotes a currently open window.
- Keyboard shortcuts are only shown if you assigned some to FSCharter functionality in your [X-Plane Settings](https://x-plane.com/manuals/desktop/#configuringkeyboardshortcuts).

### Menu Items ###

| Item               | Client's menu | Description and Links
|--------------------|---------------|------------------
| Job Window...      | Job           | Opens the [**Job** window](job-window)
| RPI Window...      | Job           | Opens the RPI window used during [Flight Continuation](/docs/flights/resuming-a-flight)                     |
| Disembark          | Job           | Disembarks the aircraft, only used when plane-specific setting **Parking Brake** is off or **Engines On** is on |
| Teleport           | Job           | Teleports the plane to the resume point during [Flight Continuation](/docs/flights/resuming-a-flight)       |
| Start Over...      | Settings      | Disregards any current flight or state and reconnects to the FSCharter platform                             |
| Retry network      | Settings      | During network connection problems this enforces a new connection attempt (otherwise periodically)       |
| Simulator Infos... | Settings      | Opens the [**Simulator Infos** window](sim-info-window) |
| Settings...        | Settings      | Opens the [**Settings**](settings-window) |
| Help               | Help          | Opens a browser showing this documentation |
| ReadMe             | Help          | Opens the local `ReadMe.html` file in a browser, which has a Changelog section listing latest changes to the plugin/client |
| Licences           | Help          | Opens the local `Licences.html` file in a browser |
| FSCharter Platform | Help          | Opens the [FSCharter platform](https://fscharter.net/) in a browser |