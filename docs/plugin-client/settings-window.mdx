---
title: Plugin/Client Settings
sidebar_label: Settings
slug: /plugin-client/settings-window
---

Here you enter your FSCharter login credentials and can tweak a number of general options:

![Settings Window](img/SettingsWnd.png)

## Login credentials

Enter the same email address and password as you use on the [FSCharter platform](https://fscharter.net/login).

| Option            | Description                                                                                   |
|-------------------|-----------------------------------------------------------------------------------------------|
| Save password...  | Saves also your password in the plugin/client's configuration file in an obfuscated (but not stricly secure) way, and makes the plugin/client **auto-login** during startup. |

## Automation

| Option            | Description                                                                                   |
|-------------------|-----------------------------------------------------------------------------------------------|
| Auto Start        | On startup, automatically open the [**Job** window](job-window) and search for an active job. |
| Auto Teleport     | Perform teleport as soon as all conditions are met, see [Resuming a flight](/docs/flights/resuming-a-flight) |
| Auto Begin Job    | When a job is found, it is automatically accepted.                                            |
| Auto Board        | When a job is accepted, it is automatically boarded.                                          |
| Auto disembark    | At the end of the flight, automatically disembark as soon as the parking brake is set and the engines are off. |

## Environment

Always use "Release (Default)". The other options are only meaningful for developers.

## Miscellaneous

Select units used by the plugin/client for _fuel_ and _weight_ display. Platform and simulators internally use the metric system, everything else is converted.

:::info Volumetric Units
Volumetric units for fuel (liter, gallons) can by principle only be approximations: Different types of fuel have different density, and density varies with temperatur. The plugin/client uses
- **X-Plane**: a fixed conversion rate for Jet-A fuel at 15°C, which is `0.804 kg/L = 6.71 lb/gal`;
- **SimConnect**: the fuel density provided by the simulator, which in turn bases on the current aircraft's fuel type.
:::

## UI Settings

The UI Settings can also be accessed from the gear wheel in any windows' title bar.

### UI Mode

The plugin/client offers a **Light** and a **Dark** mode.
![Dark Mode Example](img/DarkMode.png)

On **X-Plane**, the **Auto** setting uses darkness information from the actual simulation so it blends in well with the simulated outside world and will change mode during dusk and dawn.

In the **SimConnect** client, the **Auto** setting takes over [Windows' system setting](https://support.microsoft.com/en-us/windows/change-colors-in-windows-d26ef4d6-819a-581c-1581-493cfcc005fe).

### Font Size

To change font size, click into the control and hold the mouse button down, then carefully move the mouse left or right. As the font size will change immediately, the entire UI will rearrange, but as long as you keep the mouse button down you can keep adjusting text size.

### Opacity

...allows to define a partly transparent window background, which is especially useful with the **X-Plane** plugin so that the world still shimmers through the windows.

### Size/Position Message Wnd

Clicking `Size/Position Message Wnd` shows the **Message** window (the small text bar appearing at the top showing popup messages during the flight) in a mode that allows you to drag it to a suitable position and resize it.

You can always, whenever the window shows, drag and resize it.

## Logging

Select amount of information

| Option                    | Description                                                               |
|---------------------------|---------------------------------------------------------------------------|
| ...in popup messages      | which are the messages briefly appearing at the top.                       |
| ...in `Log.txt`           | which refers to the log file, which you may need when asking for support. |

Recommendation is to keep **popup messages** at level "Information" and **`Log.txt`** at least at level "Warning".

If you can reproduce a problem that you want to report to support, then please set **Select amount of info in `Log.txt`** to "Debug" to produce the highest level of output for analysis purposes. See [here](support) for more on requesting support.

## Plane-specific Options

For each of your aircraft that you use in your simulator, you can tweak some options here. Your current aircraft is listed first; clicking on the triangle opens the full list of all yet known aircraft and their configured options.
![Settings - Plane-specific options](img/SettingsWnd_PlaneSpecific.png)

There are so many third-party vendor aircraft available that it is impossible to test with all of them. Consequently, the plugin/client is only tested with a few stock aircraft. These options here shall help you get around issues when using complex aircraft or just because you want it this way:

| Option        | Description                                                                                                                                                       |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Location      | If off doesn't place you at the exact location when beginning a job; you will need to taxi there manually.                                                        |
| Fuel          | If on tries to load fuel as configured for the job, proportionally distributed across all tanks; if off you will need to set and distribute fuel manually.        |
| Payload       | If on tries to configure weight according to the job's payload; if off you will need to configure and distribute payload manually.                                |
| Parking Brake | If off allows to begin a job and disembark without parking brake set; might be useful for helicopters, sea planes or if parking brake/anchor is not recognized.   |
| Engines On    | If on allows to begin a job and disembark with engines on; some bush pilots like to do so.                                                                        |