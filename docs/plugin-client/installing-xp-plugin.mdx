---
title: Installing X-Plane Plugin
sidebar_label: X-Plane Plugin
slug: /plugin-client/installing-xp-plugin
---

FSCharter works by interacting with your simulator via a plugin. The plugin is responsible for placing your aircraft in the world, setting fuel and payload and recording takeoffs, flights and landings.

## Installing the X-Plane Plugin
:::info X-Plane Version
X-Plane `v11.10` or higher is required.
:::

:::info X-Plane 12 Beta
The FSCharter plugin has been run successfully in X-Plane `v12 beta` versions and users successfully perform FSCharter flights with X-Plane `12 beta`. However, as long as X-Plane 12 itself is beta software, using X-Plane `v12 beta` with FSCharter is at your own risk.
:::

FSCharter is a drop-in plugin:
- Log in to [FSCharter](https://fscharter.net) and download the plugin from the sidebar menu `Download Plugin`,
- expand the installation archive `FSCharter.zip`,
- and move the resulting `FSCharter` folder into your `<X-Plane_11_Root>/Resources/plugins` folder.

You should end up with the following file structure:

```
<X-Plane_11_Root>/
    Resources/
        plugins/
            FSCharter/
                ReadMe.html
                Licences.html
                lin_x64/
                    FSCharter.xpl
                    FSCharter-bionic.xpl
                mac_x64/
                    FSCharter.xpl
                win_x64/
                    FSCharter.xpl
```

## Connecting to FSCharter
Start X-Plane. You can log in to FSCharter using the same credentials that you use to access [fscharter.net](https://fscharter.net) by opening up the [Settings](settings-window) window (Plugins > FSCharter > Settings...):

![X-Plane Plugin menu and FSC Settings window](img/XP_SettingsLogin.png)

:::tip
Enabling _Save password in Settings_ will save your password and automatically log you in on startup.
:::

:::caution
Ensure that `Release (Default)` is selected as _Environment_ in your settings.
:::

## Troubleshooting
### The specified module could not be found (Windows only)
If you don't see FSCharter in your plugins menu and your `Log.txt` contains `Code = 126 : The specified module could not be found.` then you need to [download and install Microsoft's Visual C Redistributable](https://aka.ms/vs/16/release/vc_redist.x64.exe). It is not included in the plugin to save on download size and because in many cases it is already installed.

### Linux Plugin Version (Linux only)
There are two versions of the plugin for Linux:

* The standard version `lin_x64/FSCharter.xpl` is built based on Ubuntu 20.04 Focal Fossa.
* `FSCharter-bionic.xpl` is built based on Ubuntu 18.04 Bionic Beaver.

If the standard fails with `dlerrors` then try the Bionic one by renaming `FSCharter-bionic.xpl` to `FSCharter.xpl`.

## Updating the Plugin

The plugin informs you with a message at startup as soon as a new version of the FSC Plugin is available. Major version changes are even mandatory and you _must_ update before you can proceed.

![Update Available notification](img/UpdateAvailable.png)

Click on \[Download and Install\] to download the new plugin to your `Downloads` folder and have it installed. **See below for details per operating system.**

If you click \[Download\] only, you can install later yourself from your `Downloads` folder, [see above](#installing-the-x-plane-plugin).

### Updating on Windows

In-place updates on Windows are technically tricky as the plugin cannot overwrite itself while being executed. What happens instead is:

- The plugin spawns a separate script, which waits for `FSCharter.xpl` to become overwritable:
  ![Powershell script waiting for access to FSC Plugin](img/XP_Win_InstScriptWnd.png)
  You don't actually need to do anything in that window...just don't close it.
- The plugin informs you about your possible next steps:
  ![Initiate Reload](img/XP_Win_InitiateReload.png)
- You can now click \[Reload Plugins\], which will unload **all** plugins in X-Plane and present you the following system message:
  ![XP System Message on Reload Plugins](img/XP_Win_ReloadPlugins.png)
- The script, which had been spawned earlier, will notice within 5 seconds, that it can now overwrite, will put all files in place, and close itself. As soon as the script window has closed, click \[Understood\]. X-Plane reloads all plugins, which includes the _updated_ FSCharter Plugin, which will greet you with a notice about the updated version.

Alternatively, you can just \[Close\] the Update window in the second step and restart X-Plane at your discretion, now or later.

After the installation, both the `FSCharter.zip` archive and the installation script `InstallFSCXP.ps1` remain in your `Downloads` folder. After successful installation you can delete both.

Please take note of [general warnings below](#general-warnings-about-plugin-reload).

### Updating on MacOS and Linux

The plugin will install all files, ie. the update happens in-place and immediately. After the update you are offered to click \[Reload Plugins\], which will unload **all** plugins in X-Plane and present you the following system message:
  ![XP System Message on Reload Plugins](img/XP_Win_ReloadPlugins.png)
Just click \[Understood\] to reload all plugins, which will load the _updated_ FSC Plugin in the process.

Alternatively, you can just \[Close\] the Update window and restart X-Plane at your discretion, now or later.

Please take note of [general warnings below](#general-warnings-about-plugin-reload).

### General Warnings about Plugin Reload

:::caution Reloading Plugins and Complex Aircraft
X-Plane's "Reload Plugins" functionality reloads **all** plugins currently loaded in X-Plane. Complex aircraft typically run many aircraft-specific plugins to provide the enhanced experience. They get reloaded, too, and often don't initialize properly when reloaded this way.
:::

You can just try and figure out if your aircraft survives a reload. If you want to be on the safe side you can temporarily load Laminar's standard Cessna 172, with which reload works, even as late as when the "You can try reloading" plugin message appears. After reload, load your complex aircraft again.

## Support
Information on support has been moved [here](support).

## Tips and Tricks
* Tooltips are often available when hovering over GUI elements like checkboxes or buttons.
* For some commands you can define [keyboard shortcuts](https://x-plane.com/manuals/desktop/#configuringkeyboardshortcuts) or [joystick button assignments](https://x-plane.com/manuals/desktop/#assigningfunctionstobuttons) via the standard X-Plane functionality `(Settings > Joystick / Keyboard)`; search for "FSCharter" to find them.
* As a premium user, you can [continue an interrupted FSCharter flight](/docs/flights/resuming-a-flight) (e.g. if your simulator stopped/crashed or because you were interrupted by real life events).
