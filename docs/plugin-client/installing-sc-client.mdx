---
title: Installing the SimConnect Client for MS Flight Simulator and Prepar3D
sidebar_label: MSFS / P3D Client
slug: /plugin-client/installing-sc-client
---

FSCharter works by interacting with your simulator via a standalone SimConnect application, that connects to both your simulator (MSFS or P3D) and FSCharter. The client is responsible for placing your aircraft in the world, setting fuel and payload and recording takeoffs, flights and landings.

## Installing the Client
:::info System Requirements
- Windows 10 and
- Microsoft Flight Simulator 2020 (MSFS) or Lockheed Martin Prepar3D v5.1 (P3D)
:::

- Log in to [FSCharter](https://fscharter.net) and download the client from the sidebar menu `Download Plugin`.
- Unpack the `FSCharter_SC.zip` archive and
- run the included `FSCharter_SC.msi` installer.
- Once the installation has completed you will find an `FSCharter` icon in your Windows start menu.

## Connecting to FSCharter
Start MSFS or P3D as usual, and the FSCharter client from your Windows start menu. Wait until the client has established a connection to your simulator, saying "Connected" in the menu bar. You can log in to FSCharter using the same credentials that you use to access [fscharter.net](https://fscharter.net) by opening up the [Settings](settings-window) window (Settings > Settings...):

![FSC Client's Settings window](img/SC_SettingsLogin.png)

:::tip
Enabling _Save password in Settings_ will save your password and automatically log you in on startup.
:::

:::info How-to do an FSCharter Flight
[Youtube Video](https://youtu.be/bfvMaV_p9KQ)
:::

## Notes

### MSFS _Software Tips_ 

To see important text messages inside MSFS sent by the FSC client, you need to enable _Assistance Options > Notification > Software Tips_.
Each message needs to be clicked to go away, which is the reason why only very few messages are yet passed through to MSFS, like flight invalidation or taxiing / taking-off without having boarded an FSC job.

### Lockheed Martin Prepar3D _Pause on Task Switch_
In Prepar3D, go to `Options > General... > Application` and ensure `Pause on task switch` is **OFF**, otherwise the simulator will pause whenever you try to work with the FSCharter client, which in turn will deactivate many functions of FSCharter.

### Using a SimConnect Remote Connection
You can run `FSCharter.exe` on a remote computer and have it connect to the simulation machine over the network. This is a feature of SimConnect, not so much of FSCharter. You will need to configure:
* The server (ie. the simulator-side) to open a port for incoming remote traffic by providing a `SimConnect.xlm` file defining a connection for example with `<Protocol>=IPv4, <scope>=global, <port>=500, and <Adress>=(simulator's pc name or ip address)`. See [here](https://docs.flightsimulator.com/html/index.htm#t=Programming_Tools%2FSimConnect%2FSimConnect_XML_Definition.htm) (MSFS) or [here](https://www.prepar3d.com/SDKv5/sdk/simconnect_api/configuration_files/configuration_files_overview.html) (P3D) for more details and a template `SimConnect.xml` file.
* A firewall rule on the simulator-side if you run a firewall. Easiest would be to allow the port(s) that you configured (e.g. in Windows Defender, select `Inbound Rules`, then `Action > New Rule...`, `Port`, `TCP`, specify the ports (e.g. `500,501`), then `Allow the connection` in your `Private` network).
* The client to remotely connect to the simulator by putting a `SimConnect.cfg` file into the folder where `FSCharter.exe` lives. This files needs to define the same address, port, and protocol as open for remote connection on the server side. See [here](https://docs.flightsimulator.com/html/index.htm#t=Programming_Tools%2FSimConnect%2FSimConnect_CFG_Definition.htm) (MSFS) and again [here](https://www.prepar3d.com/SDKv5/sdk/simconnect_api/configuration_files/configuration_files_overview.html) (P3D) for more details and a template `SimConnect.cfg` file.

The FSCharter client tries all available configurations in turns, starting with `[SimConnect]` section (with no trailing digit). You can pass another configuration number as a command line parameter, so executing `FSCharter.exe 1` would load configuration `[SimConnect.1]`.

## Updating the Client

The client informs you with a message at startup as soon as a new version of the FSC Client is available. Major version changes are even mandatory and you _must_ update before you can proceed.

![Update Available notification](img/UpdateAvailable.png)

Click on \[Download and Install\] to download the new plugin to your `Downloads` folder and run its installer. The FSC Client will quit to allow the installer to do its work.

If you click \[Download\] only, you can install later yourself from your `Downloads` folder, [see above](#installing-the-client).

## Known Limitations
### Building Collision (MSFS)
If MSFS reports a collision when you accept a flight by clicking `Begin Job` then the starting position might be inside a building. This can happen for example if you accepted a job of another company, which parked the aircraft using a different simulator or with different scenery, in which there was no airport; or by accidentally placing a new aircraft inside a building.

To recover you have two options. First of all, restart your flight in MSFS, placing you aircraft at any suitable ramp or runway position. Then do one of these:

* Temporarily disable automatically setting the plane's position and taxi yourself near the boarding position:
  * In the [Settings' _Plane-specfic options_](settings-window#plane-specific-options), disable _Location_.
  * Select `Settings > Start over...` from the menu, which will restart your communication with the FSCharter platform.
  * Click `Begin Job` again, but this time your aircraft will not move.
  * If the client displays `For boarding you need to be positioned according to your current FSCharter position.` then follow the given directions to taxi towards the boarding position manually.
  * Once nearby (50m radius), set the parking brake, shut down engines, and click `Board`.
  * Consider re-enabling the _Location_ option in Settings.
* Disable MSFS's crash detection:
  * In the `Options` (separate tab in the main menu, or just hit `[Esc]` when in flying mode) go to `Assistance Options > Failure and Damage`
  * and switch `Crash Damage` to `Disabled`.
  * Select `Settings > Start over...` from the FSCharter client's menu, which will restart your communication with the FSCharter platform.
  * Click `Begin Job` again and your aircaft will be teleported into the building but won't be consdered crashed.
  * Use _Slew mode_ to navigate the plane out of the building (hit `[Y]`, then move the plane with the num pad's keys, end with `[Y]`), but not too far away from the original position to be able to board.

### Missing Payload Values (MSFS)
Payload is set automatically and applied to the aircraft's weight but MSFS does not display applied values in its Fuel/Payload dialog. This seems to be an MSFS bug.

### Payload Balancing Issues (MSFS & P3D)
Due to the nature of MSFS's and P3D's payload system, FSCharter can bring a plane out of balance and make it tilt.

In these simulators, payload is assigned to "payload stations", which can range in size from the pilot seat, via small baggage compartments, to the entire cabin or cargo compartments. But FSCharter cannot find out the max allowed weight per station! So it tries to identify single seats (pilots, individual passengers) and assigns 70kg weight each, but then distributes all remaining payload evenly across all other payload statins. This can assign too much weight to individual stations causing a fatal shift in center of gravity.

If this happens to you, you can switch off automatic payload in FSCharter's Settings, see the [_Plane-specfic options_](settings-window#plane-specific-options) at the bottom.

### Flight Continuation Altitude Issues (MSFS)
MSFS does not honor individual waypoints' altitude. For this reason, flight plans written for MSFS instead set the Cruising Altitude to be the altitude required for flight continuation.

### Message Popup Window Position (SimConnect Client)
The message popup window sometimes seems to move back to the top left corner, even if you configured it otherwise ([Settings](settings-window#sizeposition-message-wnd)). You can drag the window any time while it shows.

## Support
Information on support has been moved [here](support).

## Tips, and Tricks
* Tooltips are often available when hovering over GUI elements like checkboxes or buttons.
* As a premium user, you can [continue an interrupted FSCharter flight](/docs/flights/resuming-a-flight) (e.g. if your simulator stopped/crashed or because you were interrupted by real life events).
