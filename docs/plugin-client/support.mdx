---
title: Plugin/Client Support
sidebar_label: Support
slug: /plugin-client/support
---

**We are there to help you!** But please keep in mind that we have no remote access, neither to you computer nor to your brain. We can only work on the details you provide. The more details you provide the faster you will get meaningful responses.

Quick questions on the plugin/client functionality you best place in one of the [Discord](https://discord.gg/kqJCw8r) `#Support` channels.

If you run into problems that may require some analysis, open a ticket in the [FSCharter Feedback Centre](https://fscharter.net/feedback?tab=Feedback+Centre). To speed up things you can then also paste the URL of the feedback in the respective `#Support` channel. If you think it's the plugin/client's fault, then tag `@TwinFan` on Discord.

:::tip Good Support Request
A good support request includes intention, action, actual results, and evidences.
:::

- **Intention:** What did you _want_ to achieve?
- **Action:** What did you actually do?
- **Actual Result:** What did _instead_ happen? And why does this not fulfill your expectation?
- **Evidence:**
  - Add the **flight number**, which allows to find your data on the server;
  - Add the **log file**, which allows a more in-depth analysis of plugin/client activity especially for tricky cases. [See below](#log-files) where to find them.
  - Add **screenshots** of the simulator and the FSC plugin/client, maybe also a page of the FSC platform if you compare with information from there, so we can discuss all their actual output and figures.

## Reproduce

If you can reproduce your issue, then please do as follows:

* In the plugin/client's [**Settings**](settings-window#logging) set **Select amount of info in `Log.txt`** to "Debug".
  This adds a lot more diagnostic information into the log file, which will help analysis.
* Open the [**Simulator Infos**](sim-info-window) window before reproducing the issue.
  The window shall then be part of the screenshot and adds again more info to your request for analysis.

When the issue reoccurs take a screenshot:
* In **X-Plane** by hitting \[Shift+Space\]; you'll find the screenshot in X-Plane's `Output` folder.
* In Windows using standard tools like [Snipping](https://support.microsoft.com/en-us/windows/use-snipping-tool-to-capture-screenshots-00246869-1843-655f-f220-97299b865f6b) or by hitting \[Alt+Print Screen\].
  Find lots of ways on the internet, e.g. [here](https://www.cnet.com/tech/services-and-software/screenshots-in-windows-10-and-windows-11-7-easy-tricks/).

Add the screenshot and the resulting log file to your request as outlined above.


## Log Files

### SimConnect Client for MSFS/P3D

From the client's menu select `File > Open Log Folder...` (or go manually to `<User's Home>\AppData\Local\FSCharter`) and pick the latest `Log_*.txt` file. Log files are separated by day (the client started) and will potentially include several flights.

This log is written by the client and only contains the client's activities. It does not contain any information directly from the simulator.

The client keeps a history of five days of log files, so you will find log files also a bit later in case you forgot to attach them to your support request.

### X-Plane

Attach X-Plane's standard log file, `Log.txt`, which you find in X-Plane's main folder where also `X-Plane.exe/.app` is located.

`Log.txt` is written by X-Plane and typically also all its plugins, so it provides a pretty conclusive view on what happened in the simulator.

`Log.txt` is overwritten with every startup of X-Plane, so make sure to **make a copy** of the file as soon as you experience an issue with FSC (or, in fact, any part of X-Plane).

In case of a crash-to-desktop (CTD) of X-Plane on Windows, X-Plane writes a `.rpt` or `.dmp` to the `<X-Plane>/output/crash_reports` folder. These files are _not_ overwritten, so you can also find them days or weeks later. They can be of tremendous help when ask to analyze a CTD. Identify the right file by its file date/time. Please zip the `.rpt`/`.dmp` file and attach it to your support request.

### `FSCRawNetw.log`

If [**Amount of info in `Log.txt`**](settings-window#logging) is set to "Debug", then not only does the log file include more details, but there is also an additional file available in the same folder as the log files: `FSCRawNetw.log`. This is a protocol of every network request sent to and response received from the FSC servers. Attach this file upon request.

Unlike the other log files, this file is overwritten with every start of the plugin/client.
