---
title: Company Value & Available Finance
sidebar_label: Company Value & Finance
slug: /companies/company-value
---
## Available Finance

The amount of finance available to a company is based on the value of that company. Each company, even if they are owned by the same player, will have an individual Company Value (CV) and finance cap.

This section outlines how Company Value is calculated and how available finance works. Worked examples for these are shown in the following sub-sections.
If your question is not covered by this documentation then please don't hesitate to reach out to us on [Discord](https://discord.gg/kqJCw8r)!

### Company Value

**Company Value** is based on the **assets** and **liabilities** of that company. In simple terms, what you own and what you owe. More specifically, FSCharter calculates Company Value as `Sum of Assets` - `Finance Owed` + `Capital` + `Company Cash`


:::info Aircraft Value vs. List Price
It is very important not to mix up aircraft value with the list price. The value of the aircraft is what you would sell it for if you sold it on the second-hand market. For example, new aircraft are worth approximately 70% immediately after purchase and continue to lose value with use.
:::

#### Example 1
FSCharter Air owns a **Cessna 172 (C172)** and **Beechcraft Baron 58 (BE58)**. Each aircraft is brand new and purchased with finance with a 10% deposit on each.

##### Assets
|Asset|Value|
|-|-|
|Value of C172 after purchase|£203,560|
|Value of BE58 after purchase|£432,390|
|Bank balance|£0|
|Capital costs|£100,000 (Used to start company)|

##### Liabilities
|Liability|Value|
|-|-|
|Remaining finance for C172|£392,580|
|Remaining finance for BE58|£833,895|

Based on the information above, the Company Value can be calculated at **-£490,525** (£203,560 + £432,390 + £100,000 + £0 - £392,580 - £833,895).

Although FSCharter Air owns two aircraft, they have a negative Company Value due to the high amount of credit used.

#### Example 2
Vector Airlines also owns a **Cessna 172 (C172)** and **Beechcraft Baron 58 (BE58)**. Each aircraft is brand new however the C172 was purchased on finance with a 10% deposit, whilst the BE58 was purchased with cash.

##### Assets
|Asset|Value|
|-|-|
|Value of C172 after purchase|£203,560|
|Value of BE58 after purchase|£432,390|
|Bank balance|£0|
|Capital costs|£100,000 (Used to start company)|

##### Liabilities
|Liability|Value|
|-|-|
|Remaining finance for C172|£392,580|
|Remaining finance for BE58|£0|

Based on the information above, the Company Value can be calculated at **£343,370** (£203,560 + £432,390 + £100,000 + £0 - £392,580 - £0).

Because Vector Airlines purchased one of its aircraft outright they have no borrowing associated with it and therefore have a higher (in this case positive) Company Value.

### Available Finance

The amount of finance available to each company is based upon the Company Value, **however it is not linearly proportional**.
To assist starting players the amount that the finance changes is greater for small company values, with diminishing returns as the Company Value increases.

The minimum amount of available finance is **£2,000,000** and the maximum amount available is **£164,572,253**.

:::info Premium Players
Premium players have **£1m** added to the base finance value, which makes the minimum available finance for premium players **£3m**.
:::

Let's use the two above examples to explain available finance.

#### Example 1

FSCharter Air has the **Cessna 172 (C172)** and **Beechcraft Baron 58 (BE58)**, both purchased with finance with a 10% deposit on each. Let's assume that now each aircraft has flown 200 flights, 30% finance has been paid off on each aircraft and the company now has **£110,000** in the bank.


##### Assets
|Asset|Value|
|-|-|
|Value of C172 after 200 flights|£186,289|
|Value of BE58 after 200 flights|£395,468|
|Bank balance|£110,000|
|Capital costs|£100,000 (Used to start company)|

##### Liabilities
|Liability|Value|
|-|-|
|Remaining finance for C172|£274,806|
|Remaining finance for BE58|£583,726|

Based on the information above, the Company Value can be calculated at **-£66,775** (£186,289 + £395,468 + £100,000 + £110,000 - £274,806 - £583,726).

When Company Value is less than **£0** the finance cap is £2m (£3m for premium players). Because FSCharter Air still have **£858,532** of unpaid finance, they have **£1,141,468** of available finance from the £2m finance cap.

#### Example 2
Vector Airlines want to buy another **Cessna 208** on finance. They currently have the **C172** and **BE58**. They have the cash required for the 10% deposit (**£103,780**) and the finance needed is **£392,580**. Each of their existing aircraft have also been flown for 200 flights. The C172 was purchased with finance with a 10% deposit and the BE58 was purchased with cash. All finance has now been repaid on the C172.

They buy the plane and instantly the **available finance** shown on the banking screen changes by more than they expected. That is because the new plane has changed the Company Value in addition to increasing the finance amount used. Before purchasing the C208 the Company Value was calculated as follows:

##### Assets
|Asset|Value|
|-|-|
|Value of C172 after 200 flights|£186,289|
|Value of BE58 after 200 flights|£395,468|
|Bank balance|£103,780|
|Capital costs|£100,000 (Used to start company)|

##### Liabilities
|Liability|Value|
|-|-|
|Remaining finance for C172|£0|
|Remaining finance for BE58|£0|

Based on the information above, the Company Value before purchase was calculated at **£785,537** (£186,289 + £395,468 + £100,000 + £103,780 - £0 - £0), which resulted in **£50,185,965** of available finance.

After purchasing the C208 the Company Value is now calculated as:

##### Assets
|Asset|Value|
|-|-|
|Value of C172 after 200 flights|£186,289|
|Value of BE58 after 200 flights|£395,468|
|Value of C208 after purchase|£726,460|
|Bank balance|£0|
|Capital costs|£100,000 (Used to start company)|

##### Liabilities
|Liability|Value|
|-|-|
|Remaining finance for C172|£0|
|Remaining finance for BE58|£0|
|Remaining finance for C208|£1,401,030|

Based on the information above, the Company Value after purchase can be calculated at **£7,187** (£186,289 + £395,468 + £726,460 + £100,000 + £0 - £0 - £0 - £1,401,030), which results in **£2,431,220** of available finance.

This means that although the used finance is only **£1,401,030** the new available finance has reduced by **£47,754,745** because the Company Value dropped significantly. If the C208 was flown for 10 flights with 5% finance repaid and the bank balance still at £0, the Company Value would be **£73,309** and the available finance would be **£12,239,018**.

Hopefully this illustrates how quickly the available finance can increase with only a few flights and how (and why) the change to available finance after purchasing aircraft can be enormous. If you have any further questions then please don't hesitate to reach out to us on [Discord](https://discord.gg/kqJCw8r).