# FSCharter Documentation

FSCharter documentation is written in Markdown and uses [Docusaurus](https://docusaurus.io/docs) to render.


To get started, ensure that an SSH key is created (if one does not already exist) following the process described in the GitLab [documentation](https://docs.gitlab.com/ee/user/ssh.html#ed25519_sk-ssh-keys).

Next, install [Yarn](https://classic.yarnpkg.com/en/docs/install#windows-stable), [Node.js](https://nodejs.org/en/download/), and [Git](https://git-scm.com/). Please note that the Node.js installation may be resource intensive.

Create a local directory to place the clone from Git.  For example, `C:\gitlab-FSC` in Windows or `~/gitlab-FSC` in Unix.

From within the local directory run the `git clone` command using

```console 
git clone git@gitlab.com:Ekion/docs.git
```
 or 
 ```console
git clone https://gitlab.com/Ekion/docs.git
```

Change directory to the \docs\ folder that was created using `cd docs` or `Set-Location -Path docs`.

Next, run the following command to install dependencies.

```console
yarn install
```

Then to spin up a local development version of the documentation run the following command.

```console
yarn start
```

Documents can now be edited using a Markdown editor, such as [Ghostwriter](https://wereturtle.github.io/ghostwriter/).

Any changes made to the code will need to be done on a new branch, which will be merged into the main branch to be released.  A more detailed explanation can be found [here](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging).

Create a new branch by running this command inside the `docs` directory:

```console
git checkout -b decription-of-change
```
The branch name should match the change.

After completing the changes, run these commands inside the docs directory:

```console
git add .
```

This command tells git that you want to 'stage' the changes.
```console
git commit -m "docs: message-describing-change"
```
This will 'commit' the changes with a message. Please use the `<type>[optional scope]: <description>` format for the message. More information on this can be found [here](https://www.conventionalcommits.org/en/v1.0.0/#summary).
```console
git push --set-upstream origin decription-of-change
```
This will push the changes to GitLab. Once the changes have been pushed, the new branch can be viewed [here](https://gitlab.com/Ekion/docs/-/branches).  Follow the above procedure every time a significant change is made. 

Before making further changes to documents, resync the local development version of the documentation with the main branch using the following commands.
```console
git checkout main
```
and
```console
git pull
```