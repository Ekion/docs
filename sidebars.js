module.exports = {
  docs: [
    'overview',
    {
      type: 'category',
      label: 'Installing Plugin/Client',
      items: [
        'plugin-client/connecting-sim',
        'plugin-client/installing-sc-client',
        'plugin-client/installing-xp-plugin',
        'plugin-client/support',
      ],
    },
    {
      type: 'category',
      label: 'Demand',
      items: [
        'demand/demand-overview',
        'demand/viewing-airport-demand',
      ],
    },
    {
      type: 'category',
      label: 'Routes & Jobs',
      items: [
        'routes-and-jobs/route-overview',
        'routes-and-jobs/marketplace-overview',
        'routes-and-jobs/job-types',
        'routes-and-jobs/type-ratings',
        {
          type: 'category',
          label: 'Job Mechanics',
          items: [
            'routes-and-jobs/job-mechanics/fuel',
            'routes-and-jobs/job-mechanics/pay',
            'routes-and-jobs/job-mechanics/fees',
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'Flights',
      items: [
        'flights/recording-a-flight',
        'flights/cancelling-a-flight',
        'flights/resuming-a-flight',
        'flights/invalidated-flights',
        'flights/public-flight-board',
        'flights/ferrying-empty-aircraft',
      ],
    },
    {
      type: 'category',
      label: 'Companies',
      items: [
        'companies/company-mechanics',
        'companies/starting-a-company',
        'companies/joining-a-company',
        'companies/company-value',
        {
          type: 'category',
          label: 'Operations Cockpit',
          items: [
            {
              type: 'category',
              label: 'Managing Aircraft',
              items: [
                'companies/operations-cockpit/managing-aircraft/purchasing',
                'companies/operations-cockpit/managing-aircraft/financing',
                'companies/operations-cockpit/managing-aircraft/selling',
                'companies/operations-cockpit/managing-aircraft/maintenance',
                'companies/operations-cockpit/managing-aircraft/frozen',
              ],
            },
            'companies/operations-cockpit/opening-hubs',
            {
              type: 'category',
              label: 'Managing Routes',
              items: [
                'companies/operations-cockpit/managing-routes/creating',
                'companies/operations-cockpit/managing-routes/editing',
                'companies/operations-cockpit/managing-routes/deleting',
                'companies/operations-cockpit/managing-routes/assigning-aircraft',
              ],
            },
          ],
        },
        {
          type: 'category',
          label: 'Buildings',
          items: [
            'companies/buildings/overview',
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'XP & Achievements',
      items: [
        'xp-and-achievements/earning-xp',
        'xp-and-achievements/unlocking-achievements',
      ],
    },
    {
      type: 'category',
      label: 'Banking',
      items: [
        'banking/viewing-accounts',
        'banking/viewing-payments',
      ],
    },
    {
      type: 'category',
      label: 'Lifelines',
      items: [
        'lifelines/about-lifelines',
        'lifelines/package-handling',
        'lifelines/rewards',
        'lifelines/aid',
      ],
    },
    {
      type: 'category',
      label: 'Simulator Plugin/Client',
      items: [
        'plugin-client/overview',
        'plugin-client/job-window',
        'plugin-client/settings-window',
        'plugin-client/sim-info-window',
        'plugin-client/faq',
      ],
    },
    'marketing-materials',
  ],
};