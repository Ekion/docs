const simplePlantUML = require('@akebifiky/remark-simple-plantuml');

module.exports = {
  title: 'FSCharter Docs',
  tagline: 'Dive into the documentation to ensure you get the most out of your FSCharter experience.',
  url: 'https://docs.fscharter.net',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',
  organizationName: 'FSCharter Ltd.', // Usually your GitHub org/user name.
  projectName: 'FSCharter', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Docs',
      logo: {
        alt: 'FSCharter Logo',
        src: 'img/logo.png',
      }
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Communities',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/kqJCw8r',
            },
            {
              label: 'Reddit',
              href: 'https://reddit.com/r/fscharter',
            },
          ],
        },
        {
          title: 'Support',
          items: [
            {
              label: 'Feedback',
              to: 'https://fscharter.net/feedback',
            },
            {
              label: 'Support',
              to: 'https://fscharter.atlassian.net/servicedesk/customer/portals',
            },
          ],
        }
      ],
      copyright: `Copyright © ${new Date().getFullYear()} FSCharter Ltd.`,
    },
    colorMode: {
      defaultMode: 'light',
      disableSwitch: true,
      respectPrefersColorScheme: false,
    }
  },
  plugins: ['docusaurus-plugin-sass'],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [[simplePlantUML, {baseUrl: 'https://www.plantuml.com/plantuml/svg'}]],
          // remarkPlugins: [simplePlantUML],
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/main.css'),
        },
      },
    ],
  ],
};
