import React from 'react';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';


function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
      <Layout
          title={`Home`}
          description="FSCharter">
        <header className="px-10 md:px-16 lg:px-48 flex items-center justify-center">
          <div className="container px-0 mx-0 mb-10 pt-20 lg:mt-20">
            <h1 className="text-3xl md:text-5xl mb-3 text-primary">FSCharter <span className="font-normal text-primary">Documentation</span></h1>
            <p className="text-xl md:text-2xl font-normal text-primary-light mb-6">{siteConfig.tagline}</p>
            <a href="/docs" className="no-underline">
              <button className={' flex items-center justify-center bg-primary hover:bg-primary-dark focus:outline-none text-white h-10 px-16' +
              ' py-6' +
              ' rounded transition duration-100 ease-out focus:outline-none no-underline'}>Get Started
              </button>
            </a>
          </div>
        </header>


        <div className="w-full mt-10 mb-16 px-10 md:px-16 lg:px-48">
          <div className="container flex flex-col-reverse items-center md:flex-row px-0 py-10">
            <div className="flex-grow flex flex-col justify-center md:mr-16">
              <div className="pt-6 grid grid-cols-2 gap-10">
                <div className="bg-background-lighter rounded-lg px-6 pb-8 flex flex-col">
                  <div className="flex flex-col items-start flex-grow">
                    <h3 className="mt-8 text-lg font-medium text-primary tracking-tight">
                      <i className="fab fa-discord mr-1"></i>
                      Join us on Discord!
                    </h3>
                    <p className="mt-3 mb-5 text-base text-primary-light">
                      Become part of the community by joining our Discord server and ask anything you'd like to know whilst we complete the documentation!
                    </p>
                    <a href="https://discord.gg/kqJCw8r" target="_blank" className="flex items-center justify-center bg-primary hover:bg-primary-darker hover:no-underline focus:outline-none text-white  h-10 px-10 py-4 rounded transition duration-100 ease-out focus:outline-none flex items-center justify-center">

                      <div>Join Discord</div>
                    </a>
                  </div>
                </div>
                <div className="bg-background-lighter rounded-lg px-6 pb-8 flex flex-col">
                  <div className="flex flex-col items-start flex-grow">
                    <h3 className="mt-8 text-lg font-medium text-primary tracking-tight">
                      <i className="fab fa-discord mr-1"></i>
                      FSCharter Logos & Marketing
                    </h3>
                    <p className="mt-3 mb-5 text-base text-primary-light">
                      Download FSCharter logos for aircraft liveries, stream and recording overlays, and general publicity.
                    </p>
                    <a href="/docs/logos" className="flex items-center justify-center bg-primary hover:bg-primary-darker hover:no-underline focus:outline-none text-white h-10 px-10 py-4 rounded transition duration-100 ease-out focus:outline-none flex items-center justify-center">
                      <div>View & Download</div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
  );
}

export default Home;
